import Ember from 'ember';
import config from './config/environment';

var Router = Ember.Router.extend({
  location: config.locationType
});

Router.map(function() {
  this.resource('purchase-orders', function() {
    this.resource('purchase-order', { path: '/:purchase_order_id' }, function() {
    });
  });

  this.resource('purchase-positions', function() {
    this.resource('purchase-position', { path: '/:purchase_position_id' }, function() {
    });
  });

  this.resource('pallets', function() {
    this.resource('pallet', { path: '/:pallet_id' }, function() {
    });
  });

  this.resource('cargo-lists', function() {
    this.resource('cargo-list', { path: '/:cargo_list_id' }, function() {
    });
  });
  this.route('profile', { path: 'profile' });
});

export default Router;
