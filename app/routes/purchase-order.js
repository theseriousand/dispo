import Ember from 'ember';
// import AuthenticatedRouteMixin from 'ember-simple-auth/mixins/authenticated-route-mixin';

export default Ember.Route.extend({
  model: function(params) {
    return this.store.find('purchase-order', params.purchase_order_id);
  }
});
