import Ember from 'ember';
// import AuthenticatedRouteMixin from 'ember-simple-auth/mixins/authenticated-route-mixin';
import InfinityRoute from "ember-infinity/mixins/route";

export default Ember.Route.extend(InfinityRoute, {
  model: function() {
    // return this.store.all('product');
    // return this.store.find('pallet');
    return this.infinityModel("pallet", { perPage: 100, startingPage: 1 });
  }
});
