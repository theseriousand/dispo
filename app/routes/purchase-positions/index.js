import Ember from 'ember';
// import AuthenticatedRouteMixin from 'ember-simple-auth/mixins/authenticated-route-mixin';
import InfinityRoute from "ember-infinity/mixins/route";

export default Ember.Route.extend(InfinityRoute, {
  model: function() {
    // return this.store.all('product');
    return this.infinityModel("purchase-position", { perPage: 100, startingPage: 1 });
    // return this.store.find('purchase-position');
  }
});
