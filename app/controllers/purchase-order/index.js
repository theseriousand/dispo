import Ember from 'ember';

export default Ember.Controller.extend({
  needs: ['application'],

  selectedItems: function() {
    return [];
  }.property(),
  selectedPositions: function() {
    return [];
  }.property(),

  availableItems: Ember.computed('model.purchasePositions.@each.availableQuantity', function() {
    if (this.get('model.purchasePositions')) {
      return this.get('model.purchasePositions').filter(function(item) {
        // console.log(item.get('availableQuantity'));
        return item.get('availableQuantity') !== 0;
        // return true;
      });
    }
  }),
  actions: {
    activatePalletSearch: function() {
      this.toggleProperty('isSearching')
    },
    loadItems: function(items, pallet) {
      let self = this;
      if (pallet.get('selected')) {
        this.set('selectedBasket', null);
        this.get('selectedItems').removeObjects(items);
        console.log(this.get('selectedItems.length'));
        pallet.set('selected', false);

        items.forEach(function(item) {
          item.set('selected', false);
          self.get('selectedItems').removeObject(item);

        });

        this.get('controllers.application').set('rightSidebarToggled', false);
      } else {
        this.set('selectedBasket', pallet);
        this.get('selectedItems').addObjects(items);
        pallet.set('selected', true);

        items.forEach(function(item) {
          item.set('selected', true);
        });

        this.get('controllers.application').set('rightSidebarToggled', true);
      }
    },
    loadPosition: function(purchasePosition) {
      purchasePosition.toggleProperty('selected');
      this.get('controllers.application').set('rightSidebarToggled', true);

      this.get('selectedPositions').addObject(purchasePosition);
    },
    loadItem: function(item) {
      this.set('selectedBasket', item.get('pallet'));

      if (this.get('selectedItems').contains(item)) {
        this.get('selectedItems').removeObject(item);
        item.set('selected', false);
      } else {
        this.get('selectedItems').addObject(item);
        this.get('controllers.application').set('rightSidebarToggled', true);
        item.set('selected', true);
      }
      if (this.get('selectedItems.length') === 0) {
        this.get('controllers.application').set('rightSidebarToggled', false);
      }
      console.log(this.get('selectedItems'));
    },
    toggleRightSidebar: function() {
      this.get('controllers.application').toggleProperty('rightSidebarToggled');
    },
    assign: function(purchasePosition) {
      console.log(purchasePosition.get('id'));
    },
    add: function(lineItem) {
      let current = lineItem.get('quantity');
      lineItem.set('quantity', current + 1);
      lineItem.save();
    },
    remove: function(lineItem, pallet) {
      let current = lineItem.get('quantity');
      lineItem.set('quantity', current - 1);
      if (lineItem.get('quantity') === 0) {
        lineItem.destroyRecord();
        if (pallet.get('lineItems.length') === 0) {
          pallet.destroyRecord();
        }
      } else {
        lineItem.save();
      }
    }
  }
});
