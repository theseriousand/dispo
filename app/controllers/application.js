import Ember from 'ember';

export default Ember.Controller.extend({
  rightSidebarToggled: function() {
    return false;
  }.property(),
  primaryContentColSize: function() {
    if (this.get('rightSidebarToggled')) {
      return 'col-md-8';
    }
    return 'col-md-10';
  }.property('rightSidebarToggled')
});
