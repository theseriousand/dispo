import DS from 'ember-data';
import moment from 'moment';

export default DS.Model.extend({
  baanId: DS.attr('string'),
  deliveryDate: DS.attr('date'),
  shippingRoute: DS.belongsTo('shipping-route'),
  shippingAddress: DS.belongsTo('shipping-address'),
  purchasePositions: DS.hasMany('purchase-position', { async: true }),
  pallets: DS.hasMany('pallet', { async: true }),
  formattedDate: function() {
    return moment(this.get('deliveryDate')).format('ll');
  }.property()
});
