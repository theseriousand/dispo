import DS from 'ember-data';

export default DS.Model.extend({
  purchasePosition: DS.belongsTo('purchase-position')
});
