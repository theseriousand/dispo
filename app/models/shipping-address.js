import DS from 'ember-data';

export default DS.Model.extend({
  consigneeFull: DS.attr('string'),
  purchaseOrders: DS.hasMany('purchase-order')
});
