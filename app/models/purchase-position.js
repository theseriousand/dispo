import DS from 'ember-data';
import moment from 'moment';

export default DS.Model.extend({
  baanId: DS.attr('string'),
  deliveryDate: DS.attr('date'),
  purchaseOrder: DS.belongsTo('purchase-order'),
  zipLocation: DS.belongsTo('zip-location'),
  variants: DS.belongsTo('variant', { async: true }),
  article: DS.attr('string'),
  storageLocation: DS.attr('string'),
  availableQuantity: DS.attr('number'),
  formattedDate: function() {
    return moment(this.get('deliveryDate')).format('ll');
  }.property()
});
