import DS from 'ember-data';

export default DS.Model.extend({
  name: DS.attr('string'),
  active: DS.attr('boolean'),
  purchaseOrders: DS.hasMany('purchase-order')
});
