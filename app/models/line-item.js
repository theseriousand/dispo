import DS from 'ember-data';

export default DS.Model.extend({
  variant: DS.belongsTo('variant', { async: true }),
  purchasePosition: DS.belongsTo('purchase-position', { async: true }),
  quantity: DS.attr('number'),
  pallet: DS.belongsTo('pallet', { async: true })
});
